from typing import *

from  pandas import DataFrame
class DataFrame操作类:

    # 创建(self):

    #def 查询(self):

    @classmethod
    def 修改_按指定lambda表达式修改某行的值(self, DataFrame对象:DataFrame, 行名:str, lambda表达式:Callable)->DataFrame:
        df=DataFrame对象
        df[行名]=df[行名].apply(lambda表达式, axis=1)
        return df
    @classmethod
    def 修改_按指定lambda表达式修改某列的值(self, DataFrame对象:DataFrame,列名:str, lambda表达式:Callable)->DataFrame:
        df=DataFrame对象
        df[列名] = df[列名].apply(lambda表达式)
        return df

    @classmethod
    def 修改_按指定函数修改某行的值(self,DataFrame对象:DataFrame,行名:str,函数名:Callable,函数参数元组:Tuple)->DataFrame:
        df = DataFrame对象
        df[行名]=df[行名].apply(函数名, arg=函数参数元组,axis=1)
        return df

    @classmethod
    def 修改_按指定函数修改某列的值(self,DataFrame对象:DataFrame,列名:str,函数名:Callable,函数参数元组:Tuple)->DataFrame:
        df = DataFrame对象
        df[列名]=df[列名].apply(函数名, args=函数参数元组)
        return df

    @classmethod
    def 修改_倒序输出(self,DataFrame对象:DataFrame)->DataFrame:
        df=DataFrame对象
        df=df[::-1]
        return df

    @classmethod
    def 修改_列重命名(self, DataFrame对象:DataFrame,重命名列名字典:Dict)->DataFrame:
        df=DataFrame对象
        df=df.rename(columns=重命名列名字典)
        return  df
    @classmethod
    def 修改_设置索引列(self,DataFrame对象:DataFrame,索引列列名:str)->DataFrame:
        df=DataFrame对象
        df=df.set_index(索引列列名)
        return df

    #def 删除(self):

    #def 存取(self):

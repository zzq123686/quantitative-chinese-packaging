import tushare as ts
import backtrader  as bt
import pandas as pd
from datetime import  datetime
from Pandas操作模块 import *
class 获取数据类:



    def 获取tushare数据预设置(self):
        self.token = "4b4841f75d2e249b4db482dda50f527c87c7a19fcbb602713cd4d73e"
        ts.set_token(self.token)
        self.pro = ts.pro_api()
    def 获取股票当日开盘信息(self,股票代码,起始日期,结束日期):


        self.df = self.pro.daily(ts_code=股票代码, start_date=起始日期, end_date=结束日期)
        return self.df





class 数据预处理类:
    def __init__(self,数据):
        self.数据=数据




    def 数据预处理(self, pro=True):
        df=self.数据
        if pro:
            features = ['open', 'high', 'low', 'close', 'vol', 'trade_date']
            # convert_datetime = lambda x:datetime.strptime(x,'%Y%m%d')
            convert_datetime = lambda x: self.字符串转datetime(x)
            #df['trade_date'] = df['trade_date'].apply(convert_datetime)
            df=DataFrame操作类.修改_按指定lambda表达式修改某列的值(DataFrame对象=df, 列名='trade_date', lambda表达式=convert_datetime)
            bt_col_dict = {'vol': 'volume', 'trade_date': 'datetime'}
            df = DataFrame操作类.修改_列重命名(DataFrame对象=df, 重命名列名字典=bt_col_dict)
            df =DataFrame操作类.修改_设置索引列(DataFrame对象=df,索引列列名='datetime')
            # df.index = pd.DatetimeIndex(df.index)
        else:
            features = ['open', 'high', 'low', 'close', 'volume']
            df = df[features]
            df['openinterest'] = 0
            df.index = pd.DatetimeIndex(df.index)
        self.数据=DataFrame操作类.修改_倒序输出(df)
        return self.数据

    def 数据转换(self,开始日期,结束日期):
        self.开始日期=self.日期格式转换成datetime(开始日期,'%Y%m%d')
        self.结束日期=self.日期格式转换成datetime(结束日期,'%Y%m%d')

        data = bt.feeds.PandasData(dataname=self.数据,
                                   fromdate=self.开始日期,
                                   todate=self.结束日期)
        return  data

    def 日期格式转换成datetime(self,日期,传入格式):
        return datetime.strptime(日期,传入格式 )
    def 字符串转datetime(self,x):
        return  pd.to_datetime(str(x))






